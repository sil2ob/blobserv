package datablob

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"math/rand"
	"os"
	"strconv"
	"time"
)

func createMaps() ([]Map, error) {
	var jsonMaps JSONMaps
	var maps []Map
	filename := "assets/maps.json"

	rand.Seed(time.Now().UnixNano())

	jsonFile, errJson := os.Open(filename)
	if errJson != nil {
		return []Map{}, errJson
	}
	defer jsonFile.Close()

	byteValue, _ := ioutil.ReadAll(jsonFile)
	json.Unmarshal(byteValue, &jsonMaps)

	for _, jsonMap := range jsonMaps.Maps {
		var blobmap Map

		blobmap.Name = jsonMap.Name
		mapType := MapType{Name: jsonMap.Type}
		blobmap.Type = getOrCreateMapType(mapType)
		errInsert := insertMap(&blobmap)
		if errInsert != nil {
			return []Map{}, errInsert
		}

		maps = append(maps, blobmap)
		for _, frontSquare := range jsonMap.SquareFronts {
			position := Position{X: frontSquare[0], Y: frontSquare[1]}
			square := Square{Position: position, MapID: blobmap.ID}
			errSquare := insertSquare(&square)
			if errSquare != nil {
				fmt.Println(errSquare)
				continue
			}
			squareFront := SquareFront{SquareID: square.ID}
			errSquareFront := insertSquareFront(squareFront)
			if errSquareFront != nil {
				fmt.Println(errSquareFront)
				continue
			}

		}

		for _, fishSquare := range jsonMap.SquareWaters {
			position := Position{X: fishSquare[0], Y: fishSquare[1]}
			square := Square{Position: position, MapID: blobmap.ID}
			errSquare := insertSquare(&square)
			if errSquare != nil {
				fmt.Println(errSquare)
				continue
			}
			squareWater := SquareWater{SquareID: square.ID}
			errSquareWater := insertSquareWater(squareWater)
			if errSquareWater != nil {
				fmt.Println(errSquareWater)
				continue
			}
		}
	}

	return maps, nil
}

func addSpecies() error {
	var species JSONSpecies
	filename := "assets/species.json"

	jsonFile, err := os.Open(filename)
	if err != nil {
		return err
	}
	defer jsonFile.Close()

	byteValue, _ := ioutil.ReadAll(jsonFile)
	json.Unmarshal(byteValue, &species)

	for _, specie := range species.Species {
		realSpecie := Specie{
			Name:      specie.Name,
			Prefix:    specie.Prefix,
			MaxWeight: specie.MaxWeight,
			MaxSize:   specie.MaxSize,
			MaxSpeed:  specie.MaxSpeed,
			Rarity:    specie.Rarity,
		}

		err_specie := insertSpecies(&realSpecie)
		if err_specie != nil {
			return err_specie
		}

		for _, mapType := range specie.Maps {
			err := insertMapTypeByMaps(realSpecie.ID, mapType)
			if err != nil {
				return err
			}
		}
	}

	return nil
}

func findSpecieName(mSpecies map[string]int) string {
	n := rand.Intn(100)
	switch {
	case n < mSpecies["common"]:
		return "common"
	case n < (mSpecies["common"] + mSpecies["rare"]):
		return "rare"
	case n < 100:
		return "legend"
	}

	return ""
}

func getSpeciesByName(chosenSpecie []Specie, name string) []Specie {
	var species []Specie
	for _, specie := range chosenSpecie {
		if specie.Rarity == name {
			species = append(species, specie)
		}
	}
	return species
}

func addSeed(fish *Fish) {
	fish.Seed = string(strconv.FormatUint(rand.Uint64(), 10))
}

func createFishBySpecies(specie Specie, factors Factors) Fish {
	var fish Fish

	fish.Specie = specie
	fish.SpecieID = specie.ID

	fish.FishAsserts = FishAsserts{
		BodyShape:      (rand.Uint64() % factors.FishAsserts.BodyShape) + 1,
		BodyPattern:    (rand.Uint64() % factors.FishAsserts.BodyPattern) + 1,
		EyeShape:       (rand.Uint64() % factors.FishAsserts.EyeShape) + 1,
		EyePattern:     (rand.Uint64() % factors.FishAsserts.EyePattern) + 1,
		ArmShape:       (rand.Uint64() % factors.FishAsserts.ArmShape) + 1,
		ArmPattern:     (rand.Uint64() % factors.FishAsserts.ArmPattern) + 1,
		DorsalShape:    (rand.Uint64() % factors.FishAsserts.DorsalShape) + 1,
		DorsalPattern:  (rand.Uint64() % factors.FishAsserts.DorsalPattern) + 1,
		VentralShape:   (rand.Uint64() % factors.FishAsserts.VentralShape) + 1,
		VentralPattern: (rand.Uint64() % factors.FishAsserts.VentralPattern) + 1,
		TailShape:      (rand.Uint64() % factors.FishAsserts.TailShape) + 1,
		TailPattern:    (rand.Uint64() % factors.FishAsserts.TailPattern) + 1,
		Accessorie:     (rand.Uint64() % factors.FishAsserts.Accessorie) + 1,
	}

	fish.Weight = (rand.Uint64() % specie.MaxWeight) + 1
	fish.Size = (rand.Uint64() % specie.MaxSize) + 1
	fish.Speed = (rand.Uint64() % specie.MaxSpeed) + 1

	addSeed(&fish)

	return fish
}

func insertFishBySpecies(species []Specie) (Fish, error) {
	var mSpecies = make(map[string]int)
	var mySpecie Specie

	//get factors
	factors := getFactors()

	//build map with factors
	for _, specie := range species {
		switch specie.Rarity {
		case "common":
			mSpecies["common"] = int(factors.SpeciesCommon * 100)
		case "rare":
			mSpecies["rare"] = int(factors.SpeciesRare * 100)
		case "legend":
			mSpecies["legend"] = int(factors.SpeciesLegend * 100)
		}
	}

	//choose specie
	specieName := findSpecieName(mSpecies)
	if specieName == "" {
		return Fish{}, errors.New("error when choosing specie")
	}

	//get species matching with speciesName
	chosenSpecies := getSpeciesByName(species, specieName)

	//find the right specie
	if len(chosenSpecies) == 0 {
		return Fish{}, errors.New("error when choosing the right specie")
	} else if len(chosenSpecies) > 1 {
		mySpecie = chosenSpecies[rand.Intn(len(chosenSpecies))]
	} else {
		mySpecie = chosenSpecies[0]
	}

	//create fish by specie
	fish := createFishBySpecies(mySpecie, factors)

	err_fish := insertFish(&fish)
	if err_fish != nil {
		return Fish{}, err_fish
	}

	return fish, nil
}

func populateFishes(maps []Map) error {
	for _, myMap := range maps {
		species := getSpeciesByMapTypeId(myMap.TypeID)
		if len(species) == 0 {
			return errors.New("empty species for map")
		}

		squareWaters, err_map := getSquareWatersByMapId(myMap.ID)
		if err_map != nil {
			return err_map
		}

		for _, squareWater := range squareWaters {
			if rand.Intn(4) == 3 {
				fish, errFish := insertFishBySpecies(species)
				if errFish != nil {
					return errFish
				}

				errFishInSquare := updateSquareWaterWithFish(squareWater, fish.ID)
				if errFishInSquare != nil {
					return errFishInSquare
				}

			}
		}
	}

	return nil
}

func MoveFisherTo(id uint64, idMap uint64, x int, y int) error {
	errSquareFront := addFisherInSquareFront(id, idMap, x, y)
	if errSquareFront != nil {
		return errSquareFront
	}

	return nil
}

func addFactors() error {
	var jsonFactors JSONFactors
	filename := "assets/factors.json"

	jsonFile, err := os.Open(filename)
	if err != nil {
		return err
	}
	defer jsonFile.Close()

	byteValue, _ := ioutil.ReadAll(jsonFile)
	json.Unmarshal(byteValue, &jsonFactors)

	factors := Factors{
		SpeciesCommon: jsonFactors.FactorSpecies.Common,
		SpeciesRare:   jsonFactors.FactorSpecies.Rare,
		SpeciesLegend: jsonFactors.FactorSpecies.Legend,
		Loot:          jsonFactors.Loot,
		FishAsserts: FishAsserts{
			BodyShape:      jsonFactors.FishAssert.Body.Shape,
			BodyPattern:    jsonFactors.FishAssert.Body.Pattern,
			EyeShape:       jsonFactors.FishAssert.Eye.Shape,
			EyePattern:     jsonFactors.FishAssert.Eye.Pattern,
			ArmShape:       jsonFactors.FishAssert.Arm.Shape,
			ArmPattern:     jsonFactors.FishAssert.Arm.Pattern,
			DorsalShape:    jsonFactors.FishAssert.Dorsal.Shape,
			DorsalPattern:  jsonFactors.FishAssert.Dorsal.Pattern,
			VentralShape:   jsonFactors.FishAssert.Ventral.Shape,
			VentralPattern: jsonFactors.FishAssert.Ventral.Pattern,
			TailShape:      jsonFactors.FishAssert.Tail.Shape,
			TailPattern:    jsonFactors.FishAssert.Tail.Pattern,
			Accessorie:     jsonFactors.FishAssert.Accessorie,
		},
	}

	return insertFactors(factors)
}
