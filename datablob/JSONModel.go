package datablob

type JSONMap struct {
	Name         string   `json:"id"`
	SquareWaters [][2]int `json:"squareWaters"`
	SquareFronts [][2]int `json:"SquareFronts"`
	Type         string   `json:"type"`
}

type JSONMaps struct {
	Maps []JSONMap `json:"maps"`
}

type JSONSpecie struct {
	Name      string   `json:"name"`
	Prefix    string   `json:"prefix"`
	MaxWeight uint64   `json:"maxWeight"`
	MaxSize   uint64   `json:"maxSize"`
	MaxSpeed  uint64   `json:"maxSpeed"`
	Rarity    string   `json:"rarity"`
	Maps      []string `json:"maps"`
}

type JSONSpecies struct {
	Species []JSONSpecie `json:"species"`
}

type JSONFactorSpecies struct {
	Common float64 `json:"common"`
	Rare   float64 `json:"rare"`
	Legend float64 `json:"legend"`
}

type JSONFishAssert struct {
	Body       Assert `json:"body"`
	Eye        Assert `json:"eye"`
	Arm        Assert `json:"arm"`
	Dorsal     Assert `json:"dorsal"`
	Ventral    Assert `json:"ventral"`
	Tail       Assert `json:"tail"`
	Accessorie uint64 `json:"accessorie"`
}

type JSONFactors struct {
	FactorSpecies JSONFactorSpecies `json:"species"`
	Loot          float64           `json:"loot"`
	FishAssert    JSONFishAssert    `json:"fishAssert"`
}
