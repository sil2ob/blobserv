package datablob

func InsertLicense(license License, licenseId *uint64, user User) error {
	db, err := getDB()
	if err != nil {
		return err
	}

	license.UserID = user.ID
	license.User = user

	db.Create(&license)
	*licenseId = license.ID
	return nil
}

func InsertUser(user User, id *uint64) error {
	db, err := getDB()
	if err != nil {
		return err
	}

	db.Create(&user)
	*id = user.ID
	return nil
}

func InsertFisher(name string, license License, fisher *Fisher) error {
	db, err := getDB()
	if err != nil {
		return err
	}

	stats := Stats{Energy: 15, Power: 15, Storage: 15}

	fisher.Name = name
	fisher.SkinColor = 1
	fisher.Hairiness = 1
	fisher.Stats = stats
	fisher.License = license
	fisher.LicenseID = fisher.License.ID

	db.Create(&fisher)
	return nil
}

func insertMap(blobmap *Map) error {
	db, err := getDB()
	if err != nil {
		return err
	}

	if db.Model(&blobmap).Where("Name = ?", blobmap.Name).Updates(&blobmap).RowsAffected == 0 {
		db.Create(&blobmap)
	}
	return nil
}

func insertSquare(square *Square) error {
	db, err := getDB()
	if err != nil {
		return err
	}

	if db.Model(square).Where("X = ? and Y = ? and map_id = ?", square.Position.X, square.Position.Y, square.MapID).Updates(square).RowsAffected == 0 {
		db.Create(square)
	}
	return nil
}

func insertSpecies(specie *Specie) error {
	db, err := getDB()
	if err != nil {
		return err
	}

	if db.Model(&specie).Where("Name = ?", specie.Name).Updates(&specie).RowsAffected == 0 {
		db.Create(&specie)
	}
	return nil
}

func insertFish(fish *Fish) error {
	db, err := getDB()
	if err != nil {
		return err
	}

	if db.Model(fish).Where("seed = ?", &fish.Seed).Updates(&fish).RowsAffected == 0 {
		db.Create(&fish)
	}
	return nil
}

func insertSquareWater(squareWater SquareWater) error {
	db, err := getDB()
	if err != nil {
		return err
	}

	if db.Model(&squareWater).Where("ID = ?", squareWater.ID).Updates(&squareWater).RowsAffected == 0 {
		db.Create(&squareWater)
	}
	return nil
}

func insertSquareFront(squareFront SquareFront) error {
	db, err := getDB()
	if err != nil {
		return err
	}

	if db.Model(&squareFront).Where("ID = ?", squareFront.ID).Updates(&squareFront).RowsAffected == 0 {
		db.Create(&squareFront)
	}
	return nil
}

func getOrCreateMapType(mapType MapType) MapType {
	db, err := getDB()
	if err != nil {
		return MapType{}
	}

	if db.Model(&mapType).Where("name = ?", mapType.Name).Updates(&mapType).RowsAffected == 0 {
		db.Create(&mapType)
	}
	return mapType
}

func insertMapTypeByMaps(specieID uint64, mapTypeName string) error {
	mapType := getMapTypeByName(mapTypeName)
	if (mapType == MapType{}) {
		return DB.Error
	}
	specieByMap := SpecieByMap{SpeciesID: specieID, MapTypeID: mapType.ID}

	db, err := getDB()
	if err != nil {
		return err
	}

	db.Create(&specieByMap)

	return nil
}

func insertFactors(factors Factors) error {
	db, err := getDB()
	if err != nil {
		return err
	}

	db.Create(&factors)
	return nil
}
