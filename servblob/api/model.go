package api

type CreateUserBody struct {
	Username string
	Wallet   string
}

type CheckUserBody struct {
	Username string
	Wallet   string
}
