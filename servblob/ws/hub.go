package ws

import (
	"encoding/json"

	"blobfish.com/datablob"
)

type Hub struct {
	clients      map[*Client]bool
	Broadcast    chan []byte
	register     chan *Client
	unregister   chan *Client
	BroadcastMap chan datablob.WsMaps
}

func NewHub() *Hub {
	return &Hub{
		Broadcast:    make(chan []byte),
		register:     make(chan *Client),
		unregister:   make(chan *Client),
		clients:      make(map[*Client]bool),
		BroadcastMap: make(chan datablob.WsMaps),
	}
}

func (h *Hub) Run() {
	for {
		select {
		case client := <-h.register:
			h.clients[client] = true
		case client := <-h.unregister:
			if _, ok := h.clients[client]; ok {
				delete(h.clients, client)
				close(client.send)
			}
		case message := <-h.Broadcast:
			for client := range h.clients {
				select {
				case client.send <- message:
				default:
					close(client.send)
					delete(h.clients, client)
				}
			}
		case maps := <-h.BroadcastMap:
			for client := range h.clients {
				var rMaps datablob.WsMaps

				for _, currMap := range maps.WsMap {
					if datablob.IsUserInThisMap(client.Id, currMap.Map.ID) {
						rMaps.WsMap = append(rMaps.WsMap, currMap)
					}
				}

				jsonResponse, err := json.Marshal(rMaps)
				if err == nil {
					client.send <- jsonResponse
				}
			}
		}
	}
}
