package gameblob

import (
	"fmt"
	"strconv"

	datablob "blobfish.com/datablob"
)

func MoveFisher(msg []string, id uint64) string {
	idFisher, err := strconv.ParseUint(msg[1], 10, 64)
	idMap, err := strconv.ParseUint(msg[2], 10, 64)
	x, err := strconv.Atoi(msg[3])
	y, err := strconv.Atoi(msg[4])

	if err != nil {
		return fmt.Sprintf("{\"error\":\"incorect data format\"")
	}

	errConditions := isConditionsMoveFisherOK(id, idFisher, idMap, x, y)
	if (errConditions != Error{}) {
		return fmt.Sprintf("{\"error\":\"%s\"", errConditions)
	}

	squareFront := datablob.IsFisherInCurrentMap(idFisher, idMap)
	if (squareFront != datablob.SquareFront{}) {
		errRemove := datablob.RemoveFisherFromCurrentMap(squareFront)
		if errRemove != nil {
			return fmt.Sprintf("{\"error\":\"%s\"", errRemove)
		}
	}

	errMove := datablob.MoveFisherTo(id, idMap, x, y)
	if errMove != nil {
		fmt.Println(errMove)
		return fmt.Sprintf("{\"error\":\"%s\"", errMove)
	}

	return fmt.Sprintf("\"result\":\"ok\"")
}

func isConditionsMoveFisherOK(id uint64, idFisher uint64, idMap uint64, x int, y int) Error {
	userGotFisher := datablob.IsUserGotThisFisher(id, idFisher)
	if !userGotFisher {
		return Error{Error: nil, Message: "User don't have this Fisher"}
	}

	squareIsAvailable := datablob.IsSquareMapIsAvailable(idMap, x, y)
	if !squareIsAvailable {
		return Error{Error: nil, Message: "Square Front is not available"}
	}
	fisherIsInOtherMap := datablob.IsUserInOtherMap(idFisher, idMap)
	if fisherIsInOtherMap {
		return Error{Error: nil, Message: "Fisher already in an other map"}
	}

	return Error{}
}
