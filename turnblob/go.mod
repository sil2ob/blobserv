module blbofish.com.turnblob

require blobfish.com/servblob/ws v0.0.0
require blobfish.com/datablob v0.0.0

replace blobfish.com/datablob v0.0.0 => ../datablob
replace blobfish.com/servblob/ws v0.0.0 => ../serveblob/ws